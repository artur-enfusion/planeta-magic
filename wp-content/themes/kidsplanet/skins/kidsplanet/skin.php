<?php
/**
 * Skin file for the theme.
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }

// Theme init
if (!function_exists('themerex_action_skin_theme_setup')) {
	add_action( 'themerex_action_init_theme', 'themerex_action_skin_theme_setup', 1 );
	function themerex_action_skin_theme_setup() {

		// Add skin fonts in the used fonts list
		add_filter('themerex_filter_used_fonts',			'themerex_filter_skin_used_fonts');
		// Add skin fonts (from Google fonts) in the main fonts list (if not present).
		add_filter('themerex_filter_list_fonts',			'themerex_filter_skin_list_fonts');

		// Add skin stylesheets
		add_action('themerex_action_add_styles',			'themerex_action_skin_add_styles');
		// Add skin inline styles
		add_filter('themerex_filter_add_styles_inline',		'themerex_filter_skin_add_styles_inline');
		// Add skin responsive styles
		add_action('themerex_action_add_responsive',		'themerex_action_skin_add_responsive');
		// Add skin responsive inline styles
		add_filter('themerex_filter_add_responsive_inline',	'themerex_filter_skin_add_responsive_inline');

		// Add skin scripts
		add_action('themerex_action_add_scripts',			'themerex_action_skin_add_scripts');
		// Add skin scripts inline
		add_action('themerex_action_add_scripts_inline',	'themerex_action_skin_add_scripts_inline');

		// Add skin less files into list for compilation
		add_filter('themerex_filter_compile_less',			'themerex_filter_skin_compile_less');


		/* Color schemes
		
		// Accenterd colors
		accent1			- theme accented color 1
		accent1_hover	- theme accented color 1 (hover state)
		accent2			- theme accented color 2
		accent2_hover	- theme accented color 2 (hover state)		
		accent3			- theme accented color 3
		accent3_hover	- theme accented color 3 (hover state)		
		
		// Headers, text and links
		text			- main content
		text_light		- post info
		text_dark		- headers
		inverse_text	- text on accented background
		inverse_light	- post info on accented background
		inverse_dark	- headers on accented background
		inverse_link	- links on accented background
		inverse_hover	- hovered links on accented background
		
		// Block's border and background
		bd_color		- border for the entire block
		bg_color		- background color for the entire block
		bg_image, bg_image_position, bg_image_repeat, bg_image_attachment  - first background image for the entire block
		bg_image2,bg_image2_position,bg_image2_repeat,bg_image2_attachment - second background image for the entire block
		
		// Alternative colors - highlight blocks, form fields, etc.
		alter_text		- text on alternative background
		alter_light		- post info on alternative background
		alter_dark		- headers on alternative background
		alter_link		- links on alternative background
		alter_hover		- hovered links on alternative background
		alter_bd_color	- alternative border
		alter_bd_hover	- alternative border for hovered state or active field
		alter_bg_color	- alternative background
		alter_bg_hover	- alternative background for hovered state or active field 
		alter_bg_image, alter_bg_image_position, alter_bg_image_repeat, alter_bg_image_attachment - background image for the alternative block
		
		*/

		// Add color schemes
		themerex_add_color_scheme('original', array(

			'title'					=> __('Original', 'themerex'),

			// Accent colors
			'accent1'				=> '#ff8454',
			'accent1_hover'			=> '#ff8454',
			'accent2'				=> '#80c3d2',
			'accent2_hover'			=> '#80c3d2',
			'accent3'				=> '#f0cd3a',
			'accent3_hover'			=> '#f0cd3a',
            'accent4'				=> '#88c87b',
            'accent4_hover'			=> '#88c87b',
			
			// Headers, text and links colors
			'text'					=> '#a6a6a6',   //
			'text_light'			=> '#acb4b6',
			'text_dark'				=> '#454a4c',
			'inverse_text'			=> '#ffffff',   //
			'inverse_light'			=> '#ffffff',
			'inverse_dark'			=> '#4e7bae',   // 1-st socials
			'inverse_link'			=> '#2c9ecc',   //2-nd  socials
			'inverse_hover'			=> '#e2564b',   //1-st social in footer
			
			// Whole block border and background
			'bd_color'				=> '#f4f4f4',   //
			'bg_color'				=> '#ffffff',   //
			'bg_image'				=> themerex_get_file_url('images/bg_blue.jpg'),
			'bg_image_position'		=> 'left top',
			'bg_image_repeat'		=> 'repeat',
			'bg_image_attachment'	=> 'scroll',
			'bg_image2'				=> '',
			'bg_image2_position'	=> 'left top',
			'bg_image2_repeat'		=> 'repeat',
			'bg_image2_attachment'	=> 'scroll',
		
			// Alternative blocks (submenu items, form's fields, etc.)
			'alter_text'			=> '#ffffff',
			'alter_light'			=> '#acb4b6',
			'alter_dark'			=> '#232a34',
			'alter_link'			=> '#3a3f41',   // copyright bg
			'alter_hover'			=> '#5e6365',   //
			'alter_bd_color'		=> '#eaeaea',   //
			'alter_bd_hover'		=> '#e6e6e6',   //
			'alter_bg_color'		=> '#f2f3f4',   //
			'alter_bg_hover'		=> '#f0f0f0',
			'alter_bg_image'			=> themerex_get_file_url('images/bg_red.jpg'),
			'alter_bg_image_position'	=> 'left top',
			'alter_bg_image_repeat'		=> 'repeat',
			'alter_bg_image_attachment'	=> 'scroll',
			)
		);


		/* Font slugs:
		h1 ... h6	- headers
		p			- plain text
		link		- links
		info		- info blocks (Posted 15 May, 2015 by John Doe)
		menu		- main menu
		submenu		- dropdown menus
		logo		- logo text
		button		- button's caption
		input		- input fields
		*/

		// Add Custom fonts
		themerex_add_custom_font('h1', array(
			'title'			=> __('Heading 1', 'themerex'),
			'description'	=> '',
			'font-family'	=> '',
			'font-size' 	=> '3.889em',
			'font-weight'	=> '400',
			'font-style'	=> '',
			'line-height'	=> '1.3em',
			'margin-top'	=> '0',
			'margin-bottom'	=> '0'
			)
		);
		themerex_add_custom_font('h2', array(
			'title'			=> __('Heading 2', 'themerex'),
			'description'	=> '',
			'font-family'	=> '',
			'font-size' 	=> '2.5em',
			'font-weight'	=> '400',
			'font-style'	=> '',
			'line-height'	=> '1.3em',
			'margin-top'	=> '0',
			'margin-bottom'	=> '0'
			)
		);
		themerex_add_custom_font('h3', array(
			'title'			=> __('Heading 3', 'themerex'),
			'description'	=> '',
			'font-family'	=> '',
			'font-size' 	=> '1.944em',
			'font-weight'	=> '400',
			'font-style'	=> '',
			'line-height'	=> '1.3em',
			'margin-top'	=> '0',
			'margin-bottom'	=> '0'
			)
		);
		themerex_add_custom_font('h4', array(
			'title'			=> __('Heading 4', 'themerex'),
			'description'	=> '',
			'font-family'	=> '',
			'font-size' 	=> '1.389em',
			'font-weight'	=> '400',
			'font-style'	=> '',
			'line-height'	=> '1.3em',
			'margin-top'	=> '0',
			'margin-bottom'	=> '0'
			)
		);
		themerex_add_custom_font('h5', array(
			'title'			=> __('Heading 5', 'themerex'),
			'description'	=> '',
			'font-family'	=> '',
			'font-size' 	=> '1.111em',
			'font-weight'	=> '400',
			'font-style'	=> '',
			'line-height'	=> '1.3em',
			'margin-top'	=> '0',
			'margin-bottom'	=> '0'
			)
		);
		themerex_add_custom_font('h6', array(
			'title'			=> __('Heading 6', 'themerex'),
			'description'	=> '',
			'font-family'	=> '',
			'font-size' 	=> '1em',
			'font-weight'	=> '400',
			'font-style'	=> '',
			'line-height'	=> '1em',
			'margin-top'	=> '0',
			'margin-bottom'	=> '0'
			)
		);
		themerex_add_custom_font('p', array(
			'title'			=> __('Text', 'themerex'),
			'description'	=> '',
			'font-family'	=> 'Oswald',
			'font-size' 	=> '18px',
			'font-weight'	=> '300',
			'font-style'	=> '',
			'line-height'	=> '1.667em',
			'margin-top'	=> '',
			'margin-bottom'	=> '1em'
			)
		);
		themerex_add_custom_font('link', array(
			'title'			=> __('Links', 'themerex'),
			'description'	=> '',
			'font-family'	=> '',
			'font-size' 	=> '',
			'font-weight'	=> '',
			'font-style'	=> ''
			)
		);
		themerex_add_custom_font('info', array(
			'title'			=> __('Post info', 'themerex'),
			'description'	=> '',
			'font-family'	=> '',
			'font-size' 	=> '1em',
			'font-weight'	=> '',
			'font-style'	=> '',
			'line-height'	=> '1.333em',
			'margin-top'	=> '',
			'margin-bottom'	=> '0.65em'
			)
		);
		themerex_add_custom_font('menu', array(
			'title'			=> __('Main menu items', 'themerex'),
			'description'	=> '',
			'font-family'	=> '',
			'font-size' 	=> '',
			'font-weight'	=> '',
			'font-style'	=> '',
			'line-height'	=> '1.3em',
			'margin-top'	=> '1.45em',
			'margin-bottom'	=> '1.45em'
			)
		);
		themerex_add_custom_font('submenu', array(
			'title'			=> __('Dropdown menu items', 'themerex'),
			'description'	=> '',
			'font-family'	=> '',
			'font-size' 	=> '',
			'font-weight'	=> '',
			'font-style'	=> '',
			'line-height'	=> '1.3em',
			'margin-top'	=> '',
			'margin-bottom'	=> ''
			)
		);
		themerex_add_custom_font('logo', array(
			'title'			=> __('Logo', 'themerex'),
			'description'	=> '',
			'font-family'	=> '',
			'font-size' 	=> '2.8571em',
			'font-weight'	=> '700',
			'font-style'	=> '',
			'line-height'	=> '0.75em',
			'margin-top'	=> '0',
			'margin-bottom'	=> '0'
			)
		);
		themerex_add_custom_font('button', array(
			'title'			=> __('Buttons', 'themerex'),
			'description'	=> '',
			'font-family'	=> '',
			'font-size' 	=> '',
			'font-weight'	=> '',
			'font-style'	=> '',
			'line-height'	=> '1.3em'
			)
		);
		themerex_add_custom_font('input', array(
			'title'			=> __('Input fields', 'themerex'),
			'description'	=> '',
			'font-family'	=> '',
			'font-size' 	=> '',
			'font-weight'	=> '',
			'font-style'	=> '',
			'line-height'	=> '2.7em'
			)
		);

	}
}





//------------------------------------------------------------------------------
// Skin's fonts
//------------------------------------------------------------------------------

// Add skin fonts in the used fonts list
if (!function_exists('themerex_filter_skin_used_fonts')) {
	//add_filter('themerex_filter_used_fonts', 'themerex_filter_skin_used_fonts');
	function themerex_filter_skin_used_fonts($theme_fonts) {
		//$theme_fonts['Roboto'] = 1;
        $theme_fonts['Oswald'] = 1;
        $theme_fonts['Roboto Condensed'] = 1;
		return $theme_fonts;
	}
}

// Add skin fonts (from Google fonts) in the main fonts list (if not present).
// To use custom font-face you not need add it into list in this function
// How to install custom @font-face fonts into the theme?
// All @font-face fonts are located in "theme_name/css/font-face/" folder in the separate subfolders for the each font. Subfolder name is a font-family name!
// Place full set of the font files (for each font style and weight) and css-file named stylesheet.css in the each subfolder.
// Create your @font-face kit by using Fontsquirrel @font-face Generator (http://www.fontsquirrel.com/fontface/generator)
// and then extract the font kit (with folder in the kit) into the "theme_name/css/font-face" folder to install
if (!function_exists('themerex_filter_skin_list_fonts')) {
	//add_filter('themerex_filter_list_fonts', 'themerex_filter_skin_list_fonts');
	function themerex_filter_skin_list_fonts($list) {
		// Example:
		// if (!isset($list['Advent Pro'])) {
		//		$list['Advent Pro'] = array(
		//			'family' => 'sans-serif',																						// (required) font family
		//			'link'   => 'Advent+Pro:100,100italic,300,300italic,400,400italic,500,500italic,700,700italic,900,900italic',	// (optional) if you use Google font repository
		//			'css'    => themerex_get_file_url('/css/font-face/Advent-Pro/stylesheet.css')									// (optional) if you use custom font-face
		//			);
		// }
        if (!isset($list['Oswald']))	$list['Oswald'] = array(
            'family'=>'sans-serif',
            'link'=>'Oswald:400,300'
        );
        if (!isset($list['Roboto Condensed']))	$list['Roboto Condensed'] = array(
            'family'=>'sans-serif',
            'link'=>'Roboto+Condensed:400'
        );
		return $list;
	}
}



//------------------------------------------------------------------------------
// Skin's stylesheets
//------------------------------------------------------------------------------
// Add skin stylesheets
if (!function_exists('themerex_action_skin_add_styles')) {
	//add_action('themerex_action_add_styles', 'themerex_action_skin_add_styles');
	function themerex_action_skin_add_styles() {
		// Add stylesheet files
		themerex_enqueue_style( 'themerex-skin-style', themerex_get_file_url('skin.css'), array(), null );
		if (file_exists(themerex_get_file_dir('skin.customizer.css')))
			themerex_enqueue_style( 'themerex-skin-customizer-style', themerex_get_file_url('skin.customizer.css'), array(), null );
	}
}

// Add skin inline styles
if (!function_exists('themerex_filter_skin_add_styles_inline')) {
	//add_filter('themerex_filter_add_styles_inline', 'themerex_filter_skin_add_styles_inline');
	function themerex_filter_skin_add_styles_inline($custom_style) {
		// Todo: add skin specific styles in the $custom_style to override
		//       rules from style.css and shortcodes.css
		// Example:
		//		$scheme = themerex_get_custom_option('body_scheme');
		//		if (empty($scheme)) $scheme = 'original';
		//		$clr = themerex_get_scheme_color('accent1');
		//		if (!empty($clr)) {
		// 			$custom_style .= '
		//				a,
		//				.bg_tint_light a,
		//				.top_panel .content .search_wrap.search_style_regular .search_form_wrap .search_submit,
		//				.top_panel .content .search_wrap.search_style_regular .search_icon,
		//				.search_results .post_more,
		//				.search_results .search_results_close {
		//					color:'.esc_attr($clr).';
		//				}
		//			';
		//		}
		return $custom_style;	
	}
}

// Add skin responsive styles
if (!function_exists('themerex_action_skin_add_responsive')) {
	//add_action('themerex_action_add_responsive', 'themerex_action_skin_add_responsive');
	function themerex_action_skin_add_responsive() {
		$suffix = themerex_param_is_off(themerex_get_custom_option('show_sidebar_outer')) ? '' : '-outer';
		if (file_exists(themerex_get_file_dir('skin.responsive'.($suffix).'.css'))) 
			themerex_enqueue_style( 'theme-skin-responsive-style', themerex_get_file_url('skin.responsive'.($suffix).'.css'), array(), null );
	}
}

// Add skin responsive inline styles
if (!function_exists('themerex_filter_skin_add_responsive_inline')) {
	//add_filter('themerex_filter_add_responsive_inline', 'themerex_filter_skin_add_responsive_inline');
	function themerex_filter_skin_add_responsive_inline($custom_style) {
		return $custom_style;	
	}
}

// Add skin.less into list files for compilation
if (!function_exists('themerex_filter_skin_compile_less')) {
	//add_filter('themerex_filter_compile_less', 'themerex_filter_skin_compile_less');
	function themerex_filter_skin_compile_less($files) {
		if (file_exists(themerex_get_file_dir('skin.less'))) {
		 	$files[] = themerex_get_file_dir('skin.less');
		}
		return $files;	
	}
}



//------------------------------------------------------------------------------
// Skin's scripts
//------------------------------------------------------------------------------

// Add skin scripts
if (!function_exists('themerex_action_skin_add_scripts')) {
	//add_action('themerex_action_add_scripts', 'themerex_action_skin_add_scripts');
	function themerex_action_skin_add_scripts() {
		if (file_exists(themerex_get_file_dir('skin.js')))
			themerex_enqueue_script( 'theme-skin-script', themerex_get_file_url('skin.js'), array(), null );
		if (themerex_get_theme_option('show_theme_customizer') == 'yes' && file_exists(themerex_get_file_dir('skin.customizer.js')))
			themerex_enqueue_script( 'theme-skin-customizer-script', themerex_get_file_url('skin.customizer.js'), array(), null );
	}
}

// Add skin scripts inline
if (!function_exists('themerex_action_skin_add_scripts_inline')) {
	//add_action('themerex_action_add_scripts_inline', 'themerex_action_skin_add_scripts_inline');
	function themerex_action_skin_add_scripts_inline() {
		// Todo: add skin specific scripts
		// Example:
		// echo '<script type="text/javascript">'
		//	. 'jQuery(document).ready(function() {'
		//	. "if (THEMEREX_GLOBALS['theme_font']=='') THEMEREX_GLOBALS['theme_font'] = '" . themerex_get_custom_font_settings('p', 'font-family') . "';"
		//	. "THEMEREX_GLOBALS['theme_skin_color'] = '" . themerex_get_scheme_color('accent1') . "';"
		//	. "});"
		//	. "< /script>";
	}
}
?>