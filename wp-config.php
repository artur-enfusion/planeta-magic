<?php
/**
 * Configuraci�n b�sica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener m�s informaci�n,
 * visita la p�gina del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionar� tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

//
// Sets local dev enviroment variables
//
if($_SERVER['HTTP_HOST']=='planetamagic.dev'){
define('DB_NAME', 'planetamagic-desarrollo'); // The name of the database
define('DB_USER', 'root'); // Your MySQL username
define('DB_PASSWORD', 'root'); // �and password
define('DB_HOST', 'localhost'); // db host
define('WP_HOME', 'http://planetamagic.dev'); 
define('WP_SITEURL', 'http://planetamagic.dev');
define('LIVE', false);
define( 'WP_DEBUG', false );
}else{
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'planetamagic-desarrollo');
/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');
/** Tu contrase�a de MySQL */
define('DB_PASSWORD', '3nfu51oN_2009');
/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

define('LIVE', true);
}

if ( !defined( 'WP_HOME' ) ) {
	define('WP_HOME', 'http://creacionweb.eu/planetamagic'); 
}
	
if ( !defined( 'WP_SITEURL' ) ) {
	define('WP_SITEURL', 'http://creacionweb.eu/planetamagic');
}

/** Codificaci�n de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves �nicas de autentificaci�n.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzar� a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'v25HrB+8IO-Y#VJW+J=O*q vlDS1~G xK:gT(`&lxL+<S/uaq|4+$JO?spH+jqZl');
define('SECURE_AUTH_KEY',  '%o@k~(iT|l!|{qDw+`z|3%DW|+U/Ijok+<7_;M|*!UOvj9Zj_`<O-vQo#{sBt8GN');
define('LOGGED_IN_KEY',    'yaoO!>wb6!kC|>YPEd)?a1BN$Z2 IX]=4H$r:tQU:Yyt1hDX^pHxCW5_{pgz_nRU');
define('NONCE_KEY',        '@%K^&jmA6iZy Qk:SV7u67-e9yA[)?}a&eYI_T^Me@U]zGfb guI~mqX}cJCXxqI');
define('AUTH_SALT',        'L3Y>M[+In$?/kVeFU+vU-xI0K0F=ZIsXR{?2bONQN~epQ-9tB_2R7!r8K;9sA3%F');
define('SECURE_AUTH_SALT', 'he%V7Z <7i[klIW-X+L|Xy8+3(;ye+}KFM) @5l{wo0Jl+o:@,M8Ipn(x:FPpJG*');
define('LOGGED_IN_SALT',   'XFzAu=E*k[Ag|plITvS_O7r9u_d{:07H|.f`,,fLm1F5yEytePd;s@T2l1VuLxWC');
define('NONCE_SALT',       'qI@/i9^f6,<;us_F0}6UL6JNjxT.%Q+GV:E~^Vw6X!.<9Kj{DQbZcKYX>.FV-7:.');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo n�meros, letras y gui�n bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* �Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

